import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Lista from '../views/Lista.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/lista',
    name: 'Lista',
   // component: () => import(/* webpackChunkName: "about" */ '../views/Lista.vue')
    component: Lista
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
